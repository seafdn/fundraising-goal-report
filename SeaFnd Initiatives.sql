/* 'BHWELL','CENSUS','CLBSK','CLCCIS','CLCCP','CLCJIS','CLCLF','CLCOO','CLVDI','CPNPCAP','N2N','TSFG2G' = Core Grantmaking Programs
   'SFCREEQ','CLCROAD','CLFCSC','CLSEE','COVID' = Other SeaFdn Priorities
   'ADMIN','APPEAL' = Fuel the Foundation
   'RECOVER' = Fund for Inclusive Recovery
*/

select g.GiftFactID as FactID, g.giftdate, g.amount, f.fundidentifier, g.giftsystemid, /*attributecategory, attributedescription, */ f.refundid,       
	   CASE
		WHEN f.fundidentifier IN ('BHWELL','CENSUS','CLBSK','CLCCIS','CLCCP','CLCJIS','CLCLF','CLCOO','CLVDI','CPNPCAP','N2N','TSFG2G') THEN 'Core'
		WHEN f.fundidentifier IN ('SFCREEQ','CLCROAD','CLFCSC','CLSEE','COVID') THEN 'Other'
		WHEN f.fundidentifier IN ('ADMIN','APPEAL') THEN 'Fuel'
		WHEN f.fundidentifier = 'RECOVER' THEN 'Recover'
	END AS InitiativeType,
       CASE
           WHEN Year(g.giftdate) = Year(getdate()) THEN g.amount ELSE 0.0000
       END AS SeaGiftCurrent,
       CASE
           WHEN Year(g.giftdate) = Year(getdate())-1 THEN g.amount ELSE 0.0000
       END AS SeaGiftMinus1,
       CASE
           WHEN Year(g.giftdate) = Year(getdate())-2 THEN g.amount ELSE 0.0000
       END AS SeaGiftMinus2,
	   Null as PayeeFundType,
	   Null AS PaymentFundID,
	   Null AS PaymentFundType

from dbo.FACT_Gift g
INNER JOIN dbo.DIM_Constituent c on c.ConstituentDimID = g.ConstituentDimID 
INNER JOIN dbo.DIM_Fund f on f.FundDimID = g.funddimid
--INNER JOIN dbo.DIM_FundAttribute fa on f.funddimid = fa.funddimid 
where /*attributecategory = 'TSF Fund' and
      attributedescription = 'yes' and */
      Year(giftdate) >= Year(getdate())-2 AND
      g.gifttypeDimID NOT in (SELECT GiftTypeDimID from DIM_Gifttype
                              WHERE GiftType IN ('adjustment', 'MG Pledge', 'Pay-Cash','Pay-Stock/Property')
							  OR GiftSubType = 'Interfund') AND
      g.GiftSystemID NOT IN (SELECT GiftSystemID from FACT_GiftAttribute
                             WHERE AttributeCategory = 'Gift?' AND
                                   AttributeDescription = 'No') AND 
	f.REFundID IN ('ADMIN','APPEAL','CLCCP','CLCOO','CLBSK','CLVDI','N2N','CLCLF','CLCJIS','CLCCIS','CENSUS','TSFG2G','CPNPCAP','SFCREEQ','CLCROAD','CLFCSC','CLSEE','COVID','RECOVER','BHWELL')


UNION

SELECT --g.GrantSystemID, -- REMOVE/COMMENT OUT BEFORE PLACING IN PRODUCTION 
	gp.GrantPaymentFactID as FactID,
	gp.PayDate AS PayDate,
	NetPaymentAmount = gp.PaymentComponentAmount + (SELECT COALESCE(sum(AdjustmentAmount),0) AS Refunded 
                                                    FROM DIM_GrantAdjustment gat 
                                                    WHERE AdjustmentType = 'REFUND' 
                                                            AND gat.PaymentDimID = gp.GrantPaymentFACTID  
                                                            AND gat.FundDimID = gp.FundDimID),
	f1.REFundID AS PayeeFundID,
	gp.PaymentNumber AS PaymentNumber,
	f1.REFundID AS PayeeFundID,
	 CASE
		WHEN f1.REFundID IN ('BHWELL','CENSUS','CLBSK','CLCCIS','CLCCP','CLCJIS','CLCLF','CLCOO','CLVDI','CPNPCAP','N2N','TSFG2G') THEN 'Core'
		WHEN f1.REFundID IN ('SFCREEQ','CLCROAD','CLFCSC','CLSEE','COVID') THEN 'Other'
		WHEN f1.REFundID IN ('ADMIN','APPEAL') THEN 'Fuel'
		WHEN f1.REFundID = 'RECOVER' THEN 'Recover'
			END AS InitiativeType,

	CASE
           WHEN Year(gp.PayDate) = Year(getdate()) 
			   THEN (gp.PaymentComponentAmount + (SELECT COALESCE(sum(AdjustmentAmount),0) AS Refunded 
                                                    FROM DIM_GrantAdjustment gat 
                                                    WHERE AdjustmentType = 'REFUND' 
                                                            AND gat.PaymentDimID = gp.GrantPaymentFACTID  
                                                            AND gat.FundDimID = gp.FundDimID))
				ELSE 0.0000
       END AS DAFTransferCurrent,
       CASE
           WHEN Year(gp.PayDate) = Year(getdate())-1
			   THEN (gp.PaymentComponentAmount + (SELECT COALESCE(sum(AdjustmentAmount),0) AS Refunded 
                                                    FROM DIM_GrantAdjustment gat 
                                                    WHERE AdjustmentType = 'REFUND' 
                                                            AND gat.PaymentDimID = gp.GrantPaymentFACTID  
                                                            AND gat.FundDimID = gp.FundDimID))
				ELSE 0.0000
       END AS DAFTransferMinus1,
       CASE
           WHEN Year(gp.PayDate) = Year(getdate())-2 
			   THEN (gp.PaymentComponentAmount + (SELECT COALESCE(sum(AdjustmentAmount),0) AS Refunded 
                                                    FROM DIM_GrantAdjustment gat 
                                                    WHERE AdjustmentType = 'REFUND' 
                                                            AND gat.PaymentDimID = gp.GrantPaymentFACTID  
                                                            AND gat.FundDimID = gp.FundDimID))
				ELSE 0.0000
       END AS DAFTransferMinus2,
	   f1.REFundType as PayeeFundType,
	   f2.REFundID AS PaymentFundID,
	   f2.REFundType AS PaymentFundType                              

FROM Fact_GrantPayment gp 
     JOIN DIM_Grant g ON g.GrantDimID = gp.GrantDimID 
     JOIN DIM_Fund f2 ON gp.GEFundDimID = f2.GEFundDimID    
     LEFT JOIN DIM_Fund f1 ON g.TargetFundSystemID = f1.GEFundSystemID 
   
WHERE gp.PaymentStatus = 'Paid' 
AND g.GrantTypeDescription = 'Transfer'
AND f2.REFundType = 'Donor Advised'
AND Year(gp.PayDate) >= Year(getdate())-2