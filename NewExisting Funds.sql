SELECT g.GiftFactID,
		g.giftdate, 
       --fullname, 
       --refundid, 
       --reference, 
       amount, 
       --gifttype, 
       --giftsubtype, 
       --g.giftcodedimid, 
       --f.fefundtype, 
       CASE
           WHEN Year(f.fundstartdate) = Year(getdate())
		   THEN g.amount ELSE 0
       END AS NEWCurrent,
       CASE
           WHEN Year(f.fundstartdate) < Year(getdate())
		   AND Year(g.giftdate) = Year(getdate())
		   THEN g.amount ELSE 0
       END AS EXISTCurrent,
       CASE
           WHEN Year(f.fundstartdate) = Year(getdate())-1
				AND Year(g.giftdate) = Year(getdate())-1 
				THEN g.amount ELSE 0
       END AS NEWMinus1,
       CASE
           WHEN Year(f.fundstartdate) < Year(getdate())-1
				AND Year(g.giftdate) = Year(getdate())-1
				THEN g.amount ELSE 0
       END AS EXISTMinus1,
       CASE
           WHEN Year(f.fundstartdate) = Year(getdate())-2
				AND Year(g.giftdate) = Year(getdate())-2
				THEN g.amount ELSE 0
       END AS NEWMinus2,
       CASE
           WHEN Year(f.fundstartdate) < Year(getdate())-2 
				AND Year(g.giftdate)= Year(getdate())-2 
				THEN g.amount ELSE 0
       END AS EXISTMinus2,
       f.FundDimID,
	   f.REFundID,
	   f.REFundDescription,
	   f.fundstartdate as FundStartDate,
	   f.REFundCategory,
	   f.REFundType,
	   c.ConstituentDimID
		
FROM dbo.FACT_Gift g
     INNER JOIN dbo.DIM_Fund f on f.FundDimID = g.FundDimID
     INNER JOIN dbo.DIM_constituent c on g.constituentDIMID = c.ConstituentDIMID
     INNER JOIN dbo.DIM_GiftType gt on gt.GiftTypeDimID = g.GiftTypeDimID
     
WHERE gt.gifttype <> 'adjustment' AND 
      gt.giftsubtype <> 'event fees' AND 
      gt.giftsubtype <> 'interfund' AND 
     
-- START: added 11/26/19
      g.GiftSystemID NOT IN (SELECT GiftSystemID from FACT_GiftAttribute
                             WHERE AttributeCategory = 'Gift?' AND
                                   AttributeDescription = 'No')
-- END: added 11/26/19